(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.NpKnob = factory());
}(this, (function () { 'use strict';

//      

const round = (number, precision) => {
  const factor = 10 ** precision;
  const tempNumber = number * factor;
  const roundedTempNumber = Math.round(tempNumber);

  return roundedTempNumber / factor;
};

/*
   ________   ________  ___  __    ________   ________  ________
  |\   ___  \|\   __  \|\  \|\  \ |\   ___  \|\   __  \|\   __  \
  \ \  \\ \  \ \  \|\  \ \  \/  /|\ \  \\ \  \ \  \|\  \ \  \|\ /_
   \ \  \\ \  \ \   ____\ \   ___  \ \  \\ \  \ \  \\\  \ \   __  \
    \ \  \\ \  \ \  \___|\ \  \\ \  \ \  \\ \  \ \  \\\  \ \  \|\  \
     \ \__\\ \__\ \__\    \ \__\\ \__\ \__\\ \__\ \_______\ \_______\
      \|__| \|__|\|__|     \|__| \|__|\|__| \|__|\|_______|\|_______|

Version: 1.9.0
Author: Erwin Goossen
Website: http://navelpluisje.nl
Docs: https://bitbucket.org/Navelpluisje/npknob
Repo: https://bitbucket.org/Navelpluisje/npknob
Issues: https://bitbucket.org/Navelpluisje/npknob/issues
*/

//      
/* global CustomEvent, document, HTMLInputElement, HTMLElement, MouseEvent */

class NpKnob {

  constructor(id, options) {
    this.id = id;
    this.options = options;
    this.version = '1.9.0';

    // The DOM elements
    this.body = document.body || document.createElement('body');
    this.createKnob(id);

    // The values
    this.click = true;

    this.rotateKnob = this.handleMouseMove.bind(this);
    this.handleKnobClick = this.handleClick.bind(this);

    this.setDefaults(options || false);
    this.setEventBindings();
    this.setKnobValue();
  }

  setDefaults(options) {
    this.step = parseFloat(this.wrapper.dataset.step) || options.step || 0;
    this.min = parseFloat(this.wrapper.dataset.min) || options.min || 0;
    this.max = parseFloat(this.wrapper.dataset.max) || options.max || 1;
    this.value = parseFloat(this.wrapper.dataset.value) || options.value || 0.5;
    this.input.min = this.min.toString();
    this.input.max = this.max.toString();
    this.input.step = (10 ** (-1 * this.step)).toString();
    this.input.value = this.value.toString();
  }

  setEventBindings() {
    this.wrapper.addEventListener('mousedown', this.handleMouseDown.bind(this), false);
    this.wrapper.addEventListener('click', this.handleKnobClick);
    this.body.addEventListener('mouseup', this.handleMouseUp.bind(this), false);
    this.input.addEventListener('change', this.handleInputChange.bind(this));
  }

  handleMouseDown(event) {
    this.click = true;
    this.startY = event.screenY;
    this.setKnobActive();
    this.start = parseFloat(this.knob.style.transform.substring(7)) || 0;
    this.body.addEventListener('mousemove', this.rotateKnob, false);
  }

  handleMouseMove(event) {
    let next = event.screenY - this.startY - this.start;
    if (next > 140) {
      next = 140;
    }
    if (next < -140) {
      next = -140;
    }

    this.click = false;
    this.knob.style.transform = `rotate(${-1 * next}deg)`;
    this.setValue(this.getValueByCorner(-1 * next));
  }

  handleMouseUp() {
    this.setKnobInActive();
    this.body.removeEventListener('mousemove', this.rotateKnob, false);
  }

  handleClick(event) {
    let corner;
    const width = this.wrapper.offsetWidth / 2;
    const height = this.wrapper.offsetHeight / 2;
    const clickX = event.pageX - this.wrapper.offsetLeft;
    const clickY = event.pageY - this.wrapper.offsetTop;
    const adjacent = Math.abs(width - clickX);
    const opposite = Math.abs(height - clickY);
    const hypotenuse = Math.sqrt(adjacent ** 2 + opposite ** 2);

    if (this.click) {
      if (clickY > height) {
        corner = 90 + 360 / (Math.PI * 2) * Math.acos(adjacent / hypotenuse);
      } else {
        corner = 360 / (Math.PI * 2) * Math.asin(adjacent / hypotenuse);
      }

      if (corner > 140) {
        corner = 140;
      }

      if (clickX < width) {
        corner *= -1;
      }

      this.setValue(this.getValueByCorner(corner));
    }
  }

  handleInputChange() {
    this.setValue(parseFloat(this.input.value));
  }

  createKnob(id) {
    this.wrapper = document.getElementById(id) || document.createElement('section');
    this.wrapper.classList.add('npknob-wrapper');

    this.knob = document.createElement('div');
    this.knob.classList.add('npknob');

    this.input = document.createElement('input');
    this.input.setAttribute('type', 'number');
    this.input.name = id;

    this.wrapper.appendChild(this.knob);
    this.wrapper.appendChild(this.input);
  }

  setKnobValue(corner) {
    const newCorner = corner || 280 / (this.max - this.min) * (this.value - this.min) - 140;
    this.knob.style.transform = `rotate(${newCorner}deg)`;
    this.wrapper.dataset.value = this.getValueByCorner(newCorner).toString();
  }

  getValueByCorner(corner) {
    const calcCorner = corner || parseFloat(this.knob.style.transform.substring(7)) || 0;
    return round(+this.min + (calcCorner + 140) / 280 * (this.max - this.min), this.step);
  }

  createEvent() {
    const knobEvt = new CustomEvent('knob-rotate', {
      detail: {
        value: this.value
      }
    });
    this.knob.dispatchEvent(knobEvt);
  }

  setKnobActive() {
    this.knob.classList.add('active');
  }

  setKnobInActive() {
    this.knob.classList.remove('active');
  }

  getValue() {
    return this.value;
  }

  setValue(value) {
    if (this.value === value) {
      return false;
    }
    if (value > this.max) {
      this.wrapper.dataset.value = this.max.toString();
      this.value = this.max;
      this.input.value = this.max.toString();
    } else {
      this.wrapper.dataset.value = value.toString();
      this.value = value;
      this.input.value = value.toString();
    }
    this.createEvent();
    this.setKnobValue();
    return true;
  }
}

return NpKnob;

})));
