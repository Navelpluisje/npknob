// @flow

export const round = (number: number, precision: number): number => {
  const factor = 10 ** precision;
  const tempNumber = number * factor;
  const roundedTempNumber = Math.round(tempNumber);

  return roundedTempNumber / factor;
};

export default null;
