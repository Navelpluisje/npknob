/* global document NpKnob */

function myFunction() {
  const topNav = document.getElementById('myTopnav');
  if (topNav.classList.contains('topnav')) {
    topNav.classList.toggle('responsive');
  }
}

(() => {
  const npKnob1 = new NpKnob('npknob-1', {
    min: 0,
    max: 10,
    step: 0,
    value: 3,
  });
  const result1 = document.getElementById('npknob-1-result');
  result1.textContent = npKnob1.getValue();
  npKnob1.knob.addEventListener('knob-rotate', (evt) => {
    result1.textContent = evt.detail.value;
  });

  setTimeout(() => {
    npKnob1.setValue(8);
  }, 8000);
})();
