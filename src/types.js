// @flow
/* global UIEvent */

export type Options = {
  min?: number,
  max?: number,
  value?: number,
  step?: number,
};

export type UIEventExt = UIEvent & {
  layerX: number,
  layerY: number,
  pageX: number,
  pageY: number,
  x: number,
  y: number,
};

