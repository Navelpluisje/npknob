# NpKnob
[![npm (scoped)](https://img.shields.io/npm/v/@navelpluisje/np-knob.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/np-knob)
[![npm (scoped)](https://img.shields.io/npm/dy/np-knob.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/np-knob)
[![npm (scoped)](https://img.shields.io/npm/l/np-knob.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/np-knob)
[![npm (scoped)](https://img.shields.io/bitbucket/issues-raw/navelpluisje/npknob.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/np-knob)
[![npm (scoped)](https://img.shields.io/badge/made%20with-%E2%9D%A4-red.svg?style=flat-square)](https://navelpluisje.nl)

## About

A small pure javascript library for creating rotating knobs. It was created while I had the idea to rebuild my [BeatMachine](https://beatmachine.navelpluisje.nl).

[![NPM](https://nodei.co/npm/@navelpluisje/np-knob.png?downloads=true)](https://nodei.co/npm/@navelpluisje/np-knob/)


## Usage

Install by npm: `npm i --save @navelpluisje/np-knob`.

> There is also an `np-knob`-package, but that will be depricated soon

You can import it in your project the next ways:

* the ES6 way: `import NpKnob from 'np-knob';`
* the node way: `const NpKnob = require('np-knob');`
* the old-skool way using the script tag

In you project create an element with an id. Then add the next code:

    const knob1 = new NpKnob('your-id', options);

...and thats it. Now you can listen to the events it's fyering:

    knob1.knob.addEventListener('knob-rotate', (evt) => {
        /**
         * Print the value of the button
         */
        console.log(evt.default.value);
    });

## Documentation
### Option

* **id**: The id of the DOM-element
* **options**: Set of default options. This is an object with the following aatributes:
    - *min*: the minimum value of the knob
    - *max*: the maximum value of the knob
    - *step*: the step size in decimals.
        - step `-1` is in `0.1` step-size
        - step `0` is in `1` step-size
        - step `1` is in `10` step-size
    - *value*: the start value of the knob

### Methods

* **obj**: Returns the DOM-element of the corresponding knob
* **getValue**: Get the current value of the knob
* **setValue**: Set a new value to the knob

### Events

When rotating the knob or clicking it to set the value an event is fired.

* **knob-rotate**: Parses the value of the knob

Also the created input can be used for eventlistening. You can do it like:

    const knob1 = new NpKnob('your-id', options);
    knob1.input.addEventlistener(event, callback);


### Styling

NpKnob can easily be styled with css. Here's an example.

    .npknob + input[type="number"] {
      display: none;
    }

    .npknob-wrapper {
      display: inline-block;
      height: 120px;
      margin: 1rem auto;
      position: relative;
      width: 120px;
    }

    .npknob {
      transition: transform .5s ease;
      background: transparent;
      border-color: #92c5c8;
      border-style: solid;
      border-radius: 50%;
      border-width: 6px;
      box-sizing: border-box;
      display: block;
      position: relative;
      width: 120px;
      height: 120px;
    }

    .npknob::after {
      content: '';
      background-color: #92c5c8;
      display: block;
      height: 40%;
      left: 50%;
      position: absolute;
      top: 0;
      transform: translateX(-50%);
      width: 6px;
    }

    .npknob.active {
      transition: none;
      border-color: #a3dde0;
      box-shadow: 0 0 5px 0 #a3dde0, inset 0 0 5px 0 #a3dde0;
    }

    .npknob.active::after {
      background-color: #a3dde0;
      box-shadow: 0 0 5px 0 #a3dde0, inset 0 0 5px 0 #a3dde0;
    }


